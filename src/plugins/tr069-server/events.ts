import {
  Device,
  GetDeviceActionTableCleaned,
  IBCDeviceAction,
  ITR069EAREvents,
  TR069EARAddFile,
  TR069EARGetFile,
  TR069EARGetFileType,
  TR069EARGetUploadFileUrl,
  TR069EARGetUploadFileUrlResponse,
  TRFile,
} from "../../lib";
import { XTRA, Plugin } from "./plugin";
import { Tools } from "@bettercorp/tools/lib/Tools";
import { randomUUID } from "crypto";

export class Events {
  async init(uSelf: Plugin, xtra: XTRA): Promise<void> {
    return new Promise((resolve) => {
      uSelf.onReturnableEvent<
        TR069EARGetUploadFileUrl,
        TR069EARGetUploadFileUrlResponse
      >(
        null,
        ITR069EAREvents.GetUploadFileUrl,
        (data) =>
          new Promise((resolve, reject) => {
            const session = uSelf.RavenDB.openSession();
            if (data === undefined || data === null)
              return reject("Undefined data");
            const sessionUID = randomUUID();
            uSelf.DB.UFiles.readyUploadFile(
              data.deviceId,
              sessionUID,
              data.groupId,
              data.type,
              session
            )
              .then(async (x) => {
                let uAp = x.key.split("+");
                resolve({
                  url: `${
                    (await uSelf.getPluginConfig()).myHost
                  }/LUF/${sessionUID}/${x.id}`,
                  username: uAp[0],
                  password: uAp[1],
                });
              })
              .catch(reject);
          })
      );

      uSelf.onReturnableEvent<TR069EARGetFile, string>(
        null,
        ITR069EAREvents.GetFile,
        (data) =>
          new Promise((resolve, reject) => {
            const session = uSelf.RavenDB.openSession();
            if (data === undefined || data === null)
              return reject("Undefined data");
            uSelf.log.debug(`GET FILE: ${data.type}: ${data.id}`);
            switch (data.type) {
              case TR069EARGetFileType.deviceUploadedFile: {
                uSelf.log.info(`Request file: [deviceUploadedFile] ${data.id}`);
                uSelf.DB.UFiles.getFile(
                  data.groupId,
                  data.deviceId,
                  data.id,
                  data.key,
                  session
                )
                  .then(async (file) => {
                    uSelf.log.info(
                      `Request file: [deviceUploadedFile] ${data.id}: STREEAAAMMM->${data!.fileStreamId}`
                    );
                    //file.fileStream.on('data', x => console.log(Buffer.from(x).toString('utf8')));
                    uSelf
                      .sendStream(data!.fileStreamId, file.fileStream)
                      .then(() => resolve(file.uploadType))
                      .catch(reject);
                  })
                  .catch(reject);
                return;
              }
              case TR069EARGetFileType.serverUploadedFile: {
                uSelf.log.info(`Request file: [serverUploadedFile] ${data.id}`);
                uSelf.DB.Files.getFile(
                  data.id,
                  data.key,
                  data.filename,
                  session
                )
                  .then(async (file) => {
                    uSelf.log.info(
                      `Request file: [serverUploadedFile] ${data.id}: STREEAAAMMM`
                    );
                    uSelf
                      .sendStream(data!.fileStreamId, file.fileStream)
                      .then(() => resolve(file.fileType))
                      .catch(reject);
                  })
                  .catch(reject);
                return;
              }
            }
            reject("Unknown type");
          })
      );

      uSelf.onReturnableEvent<TR069EARAddFile, TRFile>(
        null,
        ITR069EAREvents.AddFile,
        (data) =>
          new Promise(async (resolve, reject) => {
            const session = uSelf.RavenDB.openSession();
            if (data === undefined || data === null)
              return reject("Undefined data");
            uSelf.DB.Files.addFile(
              data.filename,
              data.type,
              session,
              data.groupId,
              data.deviceId,
              data.note
            )
              .then((x) => {
                uSelf.log.info(`New uploaded file success: ${x.id}`);
                resolve(x);
              })
              .catch(reject);
          })
      );

      uSelf.onReturnableEvent<string, string>(
        null,
        ITR069EAREvents.AddFileStream,
        (data) =>
          new Promise(async (resolve, reject) => {
            const session = uSelf.RavenDB.openSession();
            if (data === undefined || data === null)
              return reject("Undefined data");
            const streamId = await uSelf.receiveStream(
              (error, stream) =>
                new Promise((streamResolve, streamReject) => {
                  if (error) return streamReject(error);
                  uSelf.DB.Files.addFileStream(data, stream, session)
                    .then((x) => {
                      uSelf.log.info(`New uploaded file success: ${x.id}`);
                      streamResolve();
                    })
                    .catch(reject);
                }),
              60
            );
            resolve(streamId);
          })
      );

      uSelf.onReturnableEvent(
        null,
        ITR069EAREvents.GetDevice,
        (data: any) =>
          new Promise((resolve, reject) => {
            uSelf.DB.Devices.getDevice(
              data.id || data || undefined,
              uSelf.RavenDB.openSession()
            )
              .then(resolve)
              .catch(reject);
          })
      );

      uSelf.onReturnableEvent(
        null,
        ITR069EAREvents.AdoptDevice,
        async (data: any) =>
          new Promise(async (resolve, reject): Promise<any> => {
            const session = uSelf.RavenDB.openSession();
            uSelf.log.debug(`ADOPT DEVICE: ${data.id}`);
            if (Tools.isNullOrUndefined(data.id))
              return reject("UNKNOWN DEVICE ID");
            let device = await (uSelf.DB.Devices.getDevice(
              data.id,
              session
            ) as Promise<Device | null>);
            if (Tools.isNullOrUndefined(device))
              return reject("UNKNOWN DEVICE");
            if (device!.adopted) return resolve(true);
            device!.adopted = true;
            device!.lastCredChange = 0;
            await uSelf.DB.Devices.updateDevice(device!.id, device!, session);
            resolve(true);
            //deviceLib.getDeviceAuthed(data.serial, data.token1, data.token2, data.token3).then(resolve).catch(reject);
          })
      );

      uSelf.onReturnableEvent(
        null,
        ITR069EAREvents.GetActions,
        (data: any) =>
          new Promise((resolve, reject) => {
            uSelf.DB.Actions.getDeviceActions(
              data.id,
              data.groupId,
              uSelf.RavenDB.openSession()
            )
              .then(resolve)
              .catch(reject);
          })
      );
      uSelf.onReturnableEvent(
        null,
        ITR069EAREvents.GetFaults,
        (data: any) =>
          new Promise((resolve, reject) => {
            uSelf.DB.Faults.getFaults(uSelf.RavenDB.openSession())
              .then(resolve)
              .catch(reject);
          })
      );
      uSelf.onReturnableEvent(
        null,
        ITR069EAREvents.ClearFaults,
        (data: any) =>
          new Promise((resolve, reject) => {
            uSelf.DB.Faults.clearFaults(uSelf.RavenDB.openSession())
              .then(resolve)
              .catch(reject);
          })
      );
      uSelf.onReturnableEvent(
        null,
        ITR069EAREvents.AddUpdateGroup,
        (data: any) =>
          new Promise((resolve, reject) => {
            uSelf.DB.Groups.createOrUpdateGroup(
              data.id,
              data.sourcePlugin,
              data.autoAdopt,
              uSelf.RavenDB.openSession()
            )
              .then(resolve)
              .catch(reject);
          })
      );
      uSelf.onReturnableEvent(
        null,
        ITR069EAREvents.GetGroupConnection,
        (data: any) =>
          new Promise((resolve, reject) => {
            uSelf.DB.Groups.getConnectionDetails(
              data.id,
              data.sourcePlugin,
              uSelf.RavenDB.openSession()
            )
              .then(resolve)
              .catch(reject);
          })
      );

      uSelf.onReturnableEvent<{
        deviceId: string;
        groupId: string;
        action: IBCDeviceAction<any>;
      }>(
        null,
        ITR069EAREvents.AddAction,
        (data: any) =>
          new Promise((resolve, reject) => {
            uSelf.DB.Actions.addDeviceAction(
              data.deviceId,
              data.groupId,
              data.action,
              uSelf.RavenDB.openSession()
            )
              .then((x) => resolve(GetDeviceActionTableCleaned(x)))
              .catch(reject);
          })
      );

      // features.onReturnableEvent(null, ITR069EAREvents.AddAction, (data: any) => new Promise((resolve, reject) => {
      //   filesLib.getFile(data.deviceId, data.groupId, data.action).then(x => resolve(GetDeviceActionTableCleaned(x))).catch(reject);
      // });
      // features.onReturnableEvent(null, BackendPluginEvents.removeDevice, (data: any) => new Promise((resolve, reject) => {
      //   deviceLib.removeDevice(data.crmId, data.serviceId, data.id, data.userId).then(resolve).catch(reject);
      // });

      // features.onReturnableEvent(null, BackendPluginEvents.updateDevice, (data: any) => new Promise((resolve, reject) => {
      //   deviceLib.updateDevice(data.crmId, data.serviceId, data.id, data.userId, data.data, data.public).then(resolve).catch(reject);
      // });

      // features.onReturnableEvent(null, BackendPluginEvents.addDevice, (data: any) => new Promise((resolve, reject) => {
      //   deviceLib.newDevice(data.crmId, data.serviceId, data.type, data.serial, data.osVersion, data.firmwareVersion, data.gatewayId, data.ip, data.gatewayReferenceId).then(resolve).catch(reject);
      // });

      // features.onReturnableEvent(null, BackendPluginEvents.addDevice, (data: any) => new Promise((resolve, reject) => {
      //   deviceLib.newDevice(data.crmId, data.serviceId, data.type, data.serial, data.osVersion, data.firmwareVersion, data.gatewayId, data.ip, data.gatewayReferenceId).then(resolve).catch(reject);
      // });

      // features.onReturnableEvent(null, BackendPluginEvents.adoptDevice, async (data: any) => new Promise((resolve, reject) => {
      //   try {
      //     let activationKey: ActivationKey = JSON.parse(Crypto.decrypt(data.key, features.getPluginConfig().newDeviceEnc.alg, features.getPluginConfig().newDeviceEnc.key));

      //     switch (activationKey.type) {
      //       case 'MIKROTIK-ROUTERBOARD':
      //         let createdDevice = await deviceLib.newDevice(activationKey.clientId, activationKey.serviceId, data.identity, {
      //           brand: 'mikrotik',
      //           make: 'routerboard',
      //           model: data.model
      //         }, data.serial, data.version, data.firmwareVersion, null, data.ip, null);

      //         await deviceLib.addLog(activationKey.clientId, activationKey.serviceId, createdDevice.id,
      //           {
      //             date: new Date().getTime(),
      //             action: LogAction.connected,
      //             desc: 'Device requested connection',
      //             meta: data
      //           });

      //         features.emitEvent('betterportal-frontend', 'send-ws-message', {
      //           data: data,
      //           action: 'networks-device-adopted',
      //           clientIdS: [activationKey.clientId]
      //         });
      //         features.emitEvent('betterportal-frontend', 'send-ws-message', {
      //           data: data,
      //           action: 'networks-device-updated',
      //           clientIdS: [activationKey.clientId]
      //         });
      //         return resolve({
      //           code: `:if ([/system package find name="tr069-client"]) do={ /tr069-client set `+
      //             `acs-url="${features.getPluginConfig().newDeviceEnc.tr069Url.replace('{KEY}', createdDevice.token)}" `+
      //             `username="${createdDevice.token2}" password="${createdDevice.token3}" check-certificate=yes `+
      //             `connection-request-password="${cryptoR.randomBytes(64).toString('hex')}" `+
      //             `connection-request-username="${cryptoR.randomBytes(64).toString('hex')}" enabled=yes periodic-inform-enabled=yes periodic-inform-interval="00:00:30" } ` +
      //             `else={ /system scheduler add interval=5m name=ActivateBetterNetworks on-event="` +
      //             `/export file=(\\"BetterNetworks-\\".[/system routerboard get serial-number].\\".rsc\\"); ` +
      //             `/delay 5s; ` +
      //             `/tool e-mail send body=\\"\\" file=(\\"BetterNetworks-\\".[/system routerboard get serial-number].\\".rsc\\") to=better-networks-mikrotik@ninja.bettercorp.co.za ` +
      //             `subject=\\"${ createdDevice.token }+${ createdDevice.token2 }+${ createdDevice.token3 }\\" ` +
      //             `from=([/system routerboard get serial-number].\\"@devices.betternetworks.co.za\\") server=${ features.getPluginConfig().mikrotik.smtpServer.ip } port=${ features.getPluginConfig().mikrotik.smtpServer.port } ` +
      //             (features.getPluginConfig().mikrotik.smtpServer.tls ? `start-tls=yes; ` : '; ') +
      //             `/delay 5s; ` +
      //             `/file remove (\\"BetterNetworks-\\".[/system routerboard get serial-number].\\".rsc\\"); ` +
      //             `/tool fetch url=\\"${ features.getPluginConfig().newDeviceEnc.url.replace('{KEY}', 'activate') }/${ createdDevice.token }/${ createdDevice.token2 }/${ createdDevice.token3 }\\" output=file dst-path=ActivateBetterNetworks.rsc; ` +
      //             `/import ActivateBetterNetworks.rsc; ` +
      //             `/delay 5s; ` +
      //             `/file remove ActivateBetterNetworks.rsc; ` +
      //             `/system scheduler remove ActivateBetterNetworks" ` +
      //             `policy=ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon start-time=startup; ` +
      //             `/log info "Please go to our portal and adopt/authorize this device."; ` +
      //             `/put "Please go to our portal and adopt/authorize this device." }`,
      //           note: 'This command adds a script that will keep trying to activate the device. Until the device is adopted/authorized it will continue. Once the device is adopted/authorized then it will setup the rest of the tools for remote management.'
      //         });
      //     }
      //     reject('UNKNOW DEVICE');
      //   } catch (exc) {
      //     reject(exc);
      //   }
      // });

      // features.onReturnableEvent(null, BackendPluginEvents.newDevice, (data: any) => new Promise((resolve, reject) => {
      //   let sendingData: ActivationKey = {
      //     clientId: data.clientId,
      //     serviceId: data.serviceId,
      //     type: data.type,
      //     time: new Date().getTime()
      //   };
      //   let activationKey = Crypto.encrypt(JSON.stringify(sendingData), features.getPluginConfig().newDeviceEnc.alg, features.getPluginConfig().newDeviceEnc.key);
      //   let message: any = null;
      //   switch (data.type) {
      //     case 'MIKROTIK-ROUTERBOARD':
      //       message = {
      //         code: `/tool fetch url=("${ features.getPluginConfig().newDeviceEnc.url.replace('{KEY}', '') }deploy\\?deploymentKey=${ activationKey }&data={\\"serial\\":\\"".[/system routerboard get serial-number]."\\",\\"model\\":\\"".[/system routerboard get model]."\\",\\"firmwareType\\":\\"".[/system routerboard get firmware-type]."\\",\\"factoryFirmware\\":\\"".[/system routerboard get factory-firmware]."\\",\\"firmwareVersion\\":\\"".[/system routerboard get current-firmware]."\\",\\"version\\":\\"".[/system package get system version]."\\",\\"identity\\":\\"".[/system identity get name]."\\"}") output=file dst-path=ActivateBetterNetworks.rsc; /import ActivateBetterNetworks.rsc; /delay 5s; /file remove ActivateBetterNetworks.rsc; `,
      //         note: 'Run this command on your terminal window on your mikrotik device. Once it deploys, you can activate it via this portal'
      //       };
      //       break;
      //   }
      //   if (message == null) return reject('UNKNOWN DEVICE TYPE');
      //   resolve(message);
      // });

      resolve();
    });
  }
}
