import { Tables, TRFileType, TRUploadedFile, TRUploadedFileStored, ITR069Events, TRUploadedFileResp } from '../../../lib';
import { v4 as UUID } from 'uuid';
import { Tools } from '@bettercorp/tools/lib/Tools';
import * as crypto from 'crypto';
import * as FS from 'fs';
import * as PATH from 'path';
import { IPluginConfigStorage } from '../sec.config';
import { XTRA, Plugin } from '../plugin';
import { IDocumentSession } from 'ravendb';

export class LibUFiles {
  private uSelf!: Plugin;
  constructor(uSelf: Plugin, xtra: XTRA) {
    this.uSelf = uSelf;
  }

  public getFile(groupId: string, deviceId: string, id: string, key: String, session: IDocumentSession): Promise<TRUploadedFileResp> {
    const self = this;
    return new Promise((resolve, reject) => {
      session.query<TRUploadedFile>({ collection: Tables.uploadedFiles })
        .whereEquals('id', id)
        .whereEquals('groupId', groupId)
        .whereEquals('deviceId', deviceId)
        .whereNotEquals('store', null)
        .whereEquals('key', key)
        .single().then(async (file: TRUploadedFile | null | any) => {
          if (Tools.isNullOrUndefined(file)) return reject('File not found! ' + id);

          const storMech = (await this.uSelf.getPluginConfig()).storage;
          if (storMech === IPluginConfigStorage.LOCAL) {
            let fileAsReq: TRUploadedFileResp = {
              ...file,
              fileStream: FS.createReadStream(PATH.join((await self.uSelf.getPluginConfig()).localIsFullPath ? (await self.uSelf.getPluginConfig()).local : PATH.join(self.uSelf.cwd, (await this.uSelf.getPluginConfig()).local), file.store.path))
            };
            return resolve(fileAsReq);
          }
          if (storMech === IPluginConfigStorage.S3) {
            const minioPath = `${ (await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.path }${ file!.store.path }`;
            self.uSelf.S3.getObject((await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.bucket, minioPath, {}, (err: any, dataStream: any) => {
              if (err) {
                return self.uSelf.log.error(err);
              }
              let fileAsReq: TRUploadedFileResp = {
                ...file!,
                fileStream: dataStream
              };
              return resolve(fileAsReq);
            });
            return;
          }
          reject('Unknown Storage Mechanism');
        }).catch((ex: any) => {
          reject(ex);
        });
    });
  }
  private getFileHash(filePath: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const storMech = (await this.uSelf.getPluginConfig()).storage;
      if (storMech === IPluginConfigStorage.LOCAL) {
        if (!FS.existsSync(filePath)) return reject(`${ filePath } does not exist!`);

        try {
          // https://gist.github.com/F1LT3R/2e4347a6609c3d0105afce68cd101561
          const hash = crypto.createHash('sha1');
          const rs = FS.createReadStream(filePath);
          rs.on('error', reject);
          rs.on('data', (chunk: any) => hash.update(chunk));
          rs.on('end', () => resolve(hash.digest('hex')));
        } catch (exzc) {
          reject(exzc);
        }
        return;
      }
      if (storMech === IPluginConfigStorage.S3) {
        return reject('getFileHash function not supported with this file storage mech.');
      }
      reject('Unknown Storage Mechanism');
    });
  }
  public readyUploadFile(deviceId: string, sessionId: string, groupId: string, type: TRFileType, session: IDocumentSession): Promise<TRUploadedFile> {
    //const self = this;
    return new Promise(async (resolve, reject) => {
      let usr = crypto.randomBytes(8).toString('hex');
      let pass = crypto.randomBytes(8).toString('hex');
      const newFile = {
        id: UUID(),
        key: usr + '+' + pass,
        time: new Date().getTime(),
        deviceId,
        groupId,
        sessionId,
        uploadType: type,
        store: null!,
        "@metadata": {
          "@collection": Tables.uploadedFiles
        }
      };

      try {
        await session.store(newFile);
        await session.saveChanges();
        resolve(newFile);
      } catch (exc) {
        reject(exc);
      }
    });
  }
  public confirmUploadFile(sessionId: string, id: string, req: any, session: IDocumentSession): Promise<TRUploadedFile> {
    const self = this;
    return new Promise((resolve, reject) => {
      session.query<TRUploadedFile>({ collection: Tables.uploadedFiles })
        .whereEquals('id', id)
        .whereEquals('sessionId', sessionId)
        .whereEquals('store', null)
        .single().then(async (file: TRUploadedFile | null | any) => {
          if (Tools.isNullOrUndefined(file)) return reject('File not found! ' + id);
          if (!Tools.isNullOrUndefined(file!.store)) return reject('File already uploaded! ' + id);

          const storMech = (await this.uSelf.getPluginConfig()).storage;
          if (storMech === IPluginConfigStorage.LOCAL) {
            let newDir = PATH.join((await self.uSelf.getPluginConfig()).localIsFullPath ? (await self.uSelf.getPluginConfig()).local : PATH.join(self.uSelf.cwd, (await this.uSelf.getPluginConfig()).local), 'ufiles');
            if (!FS.existsSync(newDir)) FS.mkdirSync(newDir);
            newDir = PATH.join(newDir, file.deviceId);
            if (!FS.existsSync(newDir)) FS.mkdirSync(newDir);
            let newFilePath = PATH.join(newDir, `${ file.id }.generic`);

            let uploadedFile: TRUploadedFileStored = {
              path: `ufiles/${ file.deviceId }/${ file.id }.generic`,
              hash: 'FAILED'
            };
            let writeStream = FS.createWriteStream(newFilePath);
            writeStream.on("close", async () => {
              try {
                self.uSelf.log.info('Finished stream');
                uploadedFile.hash = await self.getFileHash(newFilePath);
                file.store = uploadedFile;
                await session.store(file);
                await session.saveChanges();
                await self.uSelf.emitEvent(null, ITR069Events.DeviceFileUploaded, {
                  ...file,
                  //downUrl: `${ (await self.uSelf.getPluginConfig()).myHost }/LFS/${ file.groupId }/${ file.deviceId }/${ file.id }/${ file.store!.hash }`
                });
                resolve(file);
              } catch (xc) {
                reject(xc);
              }
            });
            req.pipe(writeStream);
            return;
          }
          if (storMech === IPluginConfigStorage.S3) {
            const newFilePath = `ufiles/${ file.deviceId }/${ file.id }.generic`;
            const minioPath = `${ (await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.path }${ newFilePath }`;
            self.uSelf.log.debug('upload: IPluginConfigStorage.S3', minioPath);
            self.uSelf.S3.putObject((await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.bucket,
              minioPath, req, async (error: any, etag: string) => {
                if (error) {
                  return reject(error);
                }
                self.uSelf.S3.statObject((await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.bucket, minioPath, async (e: any, stat: any) => {
                  if (e) {
                    return reject(e);
                  }
                  file.store = {
                    path: newFilePath,
                    hash: stat.etag
                  };
                  file.fileSize = stat.size;
                  
                  await session.store(file);
                  await session.saveChanges();
                  await self.uSelf.emitEvent(null, ITR069Events.DeviceFileUploaded, {
                    ...file,
                    //downUrl: `${ (await self.uSelf.getPluginConfig()).myHost }/LFS/${ file.groupId }/${ file.deviceId }/${ file.id }/${ file.store!.hash }`
                  });
                  resolve(file);
                })
              });
            return;
          }
          reject('Unknown Storage Mechanism');
        }).catch((ex: any) => {
          reject(ex);
        });
    });
  }
}
