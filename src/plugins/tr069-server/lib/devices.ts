import { Tables, Device, DeviceInfo, Session, ITR069Events, GetDeviceTableCleaned, Group } from '../../../lib';
import { v4 as UUID } from 'uuid';
import { Tools } from '@bettercorp/tools/lib/Tools';
const JSONCheckSum = require('json-checksum');
import * as crypto from 'crypto';
import { XTRA, Plugin } from '../plugin';
import { IDocumentSession } from 'ravendb';

export class LibDevices {
  private uSelf!: Plugin;
  constructor(uSelf: Plugin, xtra: XTRA) {
    this.uSelf = uSelf;
  }
  private getDeviceKey(info: DeviceInfo): string {
    return JSONCheckSum(info);
  }
  getDeviceFromAuth(username: string, password: string, urlKey: string, groupId: string, info: DeviceInfo, clientIp: string, session: IDocumentSession): Promise<Device | null> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      try {
        self.uSelf.log.debug(`getDeviceFromAuth`, username, password, urlKey, groupId, info, clientIp);
        let group = await self.uSelf.DB.Groups.getGroupByAuth(username, password, groupId, urlKey, session);
        self.uSelf.log.debug(group);

        if (Tools.isNullOrUndefined(group)) return reject('UNKNOWN GROUP:getDeviceFromAuth');
        if (Tools.isNullOrUndefined(info)) return reject('UNKNOWN INFO');

        let deviceKey = self.getDeviceKey(info);

        let foundDevices: Device | Array<Device> | null = await session.query<Device[]>({ collection: Tables.devices })
          .whereEquals('groupId', group!.id)
          .whereEquals('deviceKey', deviceKey)
          .firstOrNull();

        self.uSelf.log.debug('foundDevices', foundDevices);

        if (Tools.isNullOrUndefined(foundDevices) || (Tools.isArray(foundDevices) && foundDevices!.length === 0)) {
          foundDevices = await self.newDevice(info, deviceKey, group!.id, clientIp, session);
        }

        if (Tools.isArray(foundDevices))
          return resolve((foundDevices as any)[0]);
        resolve(foundDevices as any || null);
      } catch (exc) {
        reject(exc);
      }
    });
  }
  getDeviceFromAuthV3(username: string, password: string, urlKey: string, info: DeviceInfo, clientIp: string, session: IDocumentSession): Promise<Device | null> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      try {
        let group = await self.uSelf.DB.Groups.getGroupByAuthV2(username, password, urlKey, session);

        if (group === undefined || group === null) return reject(`UNKNOWN GROUP:getDeviceFromAuthV3/${ username }/${ password }/${ urlKey }`);
        if (Tools.isNullOrUndefined(info)) return reject('UNKNOWN INFO');

        self.uSelf.log.debug('FOUND GROUP', group);
        let deviceKey = self.getDeviceKey(info);

        let foundDevices: Device | Array<Device> | null = await session.query<Device[]>({ collection: Tables.devices })
          .whereEquals('groupId', group.id)
          .whereEquals('deviceKey', deviceKey)
          .firstOrNull();

        if (Tools.isNullOrUndefined(foundDevices) || (Tools.isArray(foundDevices) && foundDevices!.length === 0)) {
          foundDevices = await self.newDevice(info, deviceKey, group.id, clientIp, session);
        }

        if (Tools.isArray(foundDevices))
          return resolve((foundDevices as any)[0]);
        resolve(foundDevices as any || null);
      } catch (exc) {
        reject(exc);
      }
    });
  }

  getDeviceFromSession(sessionid: string, docSession: IDocumentSession): Promise<Device | null> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      let session: Session | null = await self.uSelf.DB.Sessions.getSession(sessionid);
      if (Tools.isNullOrUndefined(session)) return reject('Unknown session');
      resolve(await self.getDevice(session!.deviceId!, docSession));
    });
  }

  getDevice(id: String, session: IDocumentSession): Promise<Device | null> {
    return session.query<Device>({ collection: Tables.devices })
      .whereEquals('id', id).firstOrNull();
  }
  credUpdate(deviceId: string, session: IDocumentSession): Promise<Device> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      let device = (await self.getDevice(deviceId, session)) as Device | null;
      if (Tools.isNullOrUndefined(device)) return reject('UNKNOWN DEVICE');

      device!.lastCredChange = new Date().getTime();
      device!.crp = crypto.randomBytes(65).toString('hex');
      device!.cru = crypto.randomBytes(65).toString('hex');

      self.updateDevice(device!.id, device as Device, session).then(resolve).catch(reject);
    });
  }

  newDevice(info: DeviceInfo, deviceKey: string, groupId: string, clientIp: string, session: IDocumentSession): Promise<Device> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      let group = await (self.uSelf.DB.Groups.getGroups(groupId, session) as Promise<Group>);
      if (Tools.isNullOrUndefined(group)) return reject('UNKNOW GROUP');
      let newDevice = {
        tr069LastInformTime: 0,
        id: UUID(),
        device: info,
        deviceKey: deviceKey,
        groupId: groupId,
        lastConnectDate: new Date().getTime(),
        lastCredChange: -1,
        lastConnectIP: clientIp,
        crp: '',
        cru: '',
        adopted: group.autoAdopt,
        synced: false,
        deleted: false,
        liveDebug: false,
        "@metadata": {
          "@collection": Tables.devices
        }
      };
      try {
        await session.store(newDevice);
        await session.saveChanges();
        await self.uSelf.emitEvent(null, ITR069Events.DeviceAdded, GetDeviceTableCleaned(newDevice));
        if (group.autoAdopt === true) {
          setTimeout(async () => {
            await self.uSelf.emitEvent(null, ITR069Events.DeviceAdopted, GetDeviceTableCleaned(newDevice));
          }, 5000);
        }
        resolve(newDevice);
      } catch (exc) {
        reject(exc);
      }
    });
  }
  updateDevice(id: string, device: Device, session: IDocumentSession): Promise<Device> {
    //const self = this;
    return new Promise(async (resolve, reject) => {
      try {
        await session.store(device);
        await session.saveChanges();
        resolve(device);
      } catch (exc) {
        reject(exc);
      }
    });
  }
}
