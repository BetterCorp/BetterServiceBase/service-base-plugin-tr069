import {
  Tables,
  Session,
  DeviceAction,
  DeviceActionType,
  TRFileType,
  DeviceActionStatus,
  ITR069Events,
  GetDeviceActionTableCleaned,
  IBCDeviceAction,
} from "../../../lib";
import { v4 as UUID } from "uuid";
import { Tools } from "@bettercorp/tools/lib/Tools";
import { XTRA, Plugin } from "../plugin";
import { IDocumentSession } from "ravendb";
import { DSC } from "../deviceSpecificConfig";

export class LibActions {
  private uSelf!: Plugin;
  constructor(uSelf: Plugin, xtra: XTRA) {
    this.uSelf = uSelf;
  }

  addDeviceAction(
    deviceId: string,
    groupId: string,
    actionp: IBCDeviceAction,
    docSession: IDocumentSession
  ): Promise<DeviceAction> {
    let action: DeviceAction = {
      id: UUID(),
      initAfter: (actionp.initAfter || null)!,
      data: (actionp.data || null)!,
      action: actionp.action,
      extraMeta: actionp.extraMeta,
      groupId: groupId,
      deviceId: deviceId,
      status: DeviceActionStatus.ready,
      createdTime: new Date().getTime(),
      triggeredTime: null,
      completedTime: null,
      faultId: null,
      responses: [],
    };

    const self = this;
    return new Promise(async (resolve, reject) => {
      try {
        (action as any)["@metadata"] = {
          "@collection": Tables.actions,
        };
        await docSession.store(action);
        await docSession.saveChanges();
        await self.uSelf.emitEvent(
          null,
          ITR069Events.DeviceActionAdded,
          GetDeviceActionTableCleaned(action)
        );
        resolve(action);
      } catch (exc) {
        reject(exc);
      }
    });
  }
  getDeviceActions(
    deviceId: string,
    groupId: string,
    docSession: IDocumentSession
  ): Promise<Array<DeviceAction>> {
    return new Promise(async (resolve, reject) => {
      try {
        let thirtyDaysAgo = new Date().getTime() - 1000 * 60 * 60 * 24 * 30;
        docSession
          .query<DeviceAction>({ collection: Tables.actions })
          .whereEquals("deviceId", deviceId)
          .andAlso()
          .whereEquals("groupId", groupId)
          .andAlso()
          .openSubclause()
          .whereEquals("initAfter", null)
          .orElse()
          .whereLessThan("initAfter", thirtyDaysAgo)
          .closeSubclause()
          .orderBy("createdTime")
          .take(50)
          .all()
          .then(resolve)
          .catch(reject);
      } catch (exc) {
        reject(exc);
      }
    });
  }
  getAvailableDeviceAction(
    session: Session,
    req: any,
    docSession: IDocumentSession
  ): Promise<DeviceAction> {
    const self = this;
    /*
    "SOAP-ENV:Body": [{
      "cwmp:RebootResponse": [""]
    }]
    */
    return new Promise(async (resolve, reject) => {
      let respBody: any = null;
      let trespBody: any = null;
      try {
        trespBody =
          req[`${session.envKeyType}:Envelope`][
            `${session.envKeyType}:Body`
          ];
        respBody = Object.keys(trespBody)[0];
      } catch (exc) {}
      self.uSelf.log.debug('Received Action/Action Response: ', trespBody, respBody)
      let specialRequest = self.getSpecialActionResponse(respBody, req);
      if (!Tools.isNullOrUndefined(specialRequest))
        return resolve(specialRequest!);

      let now = new Date().getTime();
      self.uSelf.log.debug("getAvailableDeviceAction:", session.deviceId, now);
      try {
        let action = await docSession
          .query<DeviceAction>({ collection: Tables.actions })
          .whereEquals("deviceId", session.deviceId)
          .andAlso()
          .whereEquals("status", DeviceActionStatus.triggered)
          .orderBy("createdTime")
          .firstOrNull();

        if (action === null) {
          action = await docSession
            .query<DeviceAction>({ collection: Tables.actions })
            .whereEquals("deviceId", session.deviceId)
            .andAlso()
            .openSubclause()
            .whereEquals("status", DeviceActionStatus.ready)
            .orElse()
            .whereEquals("status", DeviceActionStatus.triggered)
            .closeSubclause()
            .andAlso()
            .openSubclause()
            .whereEquals("initAfter", null)
            .orElse()
            .whereLessThan("initAfter", now)
            .closeSubclause()
            .orderBy("createdTime")
            .firstOrNull();
        }
        self.uSelf.log.debug(
          "getAvailableDeviceAction:",
          session.deviceId,
          now,
          action
        );
        if (action === null) return resolve(null!);

        action.responses.push({
          _time: new Date().getTime(),
          ...trespBody,
        });

        let reqKeys = await self.getActionBodyType(
          action!.action,
          action!.data
        );
        if (reqKeys.response === respBody) {
          action!.status = DeviceActionStatus.completed;
          action!.completedTime = now;
          await docSession.store(action);
          await docSession.saveChanges();
          await self.uSelf.emitEvent(
            null,
            ITR069Events.DeviceActionCompleted,
            GetDeviceActionTableCleaned(action!)
          );
          return resolve(null!); //self.getAvailableDeviceAction(session, req).then(resolve).catch(reject);
        }

        action!.status = DeviceActionStatus.triggered;
        action!.triggeredTime = now;
        await docSession.store(action);
        await docSession.saveChanges();
        await self.uSelf.emitEvent(
          null,
          ITR069Events.DeviceActionTriggered,
          GetDeviceActionTableCleaned(action!)
        );

        return resolve(action!);
      } catch (xc) {
        if (
          !Tools.isNullOrUndefined(xc) &&
          (xc as any).message === "Expected at least one result."
        )
          return resolve(null!);
        reject(xc);
      }
    });
  }
  private getSpecialActionResponse(key: string, req: any): DeviceAction | null {
    if (Tools.isNullOrUndefined(key)) return null;
    switch (key) {
      case "cwmp:GetRPCMethodsResponse":
        return null;
      case "cwmp:TransferComplete":
        return {
          id: "TRANSFER-RESPONSE",
          createdTime: 0,
          triggeredTime: 0,
          faultId: null,
          completedTime: null,
          deviceId: "",
          groupId: "",
          status: DeviceActionStatus.ready,
          action: DeviceActionType.specialAction,
          data: {
            body: {
              request: 'cwmp:TransferCompleteResponse',
              response: 'cwmp:TransferCompleteResponse'
            }
          },
          responses: [],
        };
    }
    return null;
  }
  public getActionBody<T = any>(
    actionType: DeviceActionType,
    data: T,
    url: string,
    session: Session,
    docSession: IDocumentSession
  ) {
    const self = this;
    return new Promise((resolve, reject) => {
      self.uSelf.log.debug("getActionBodyType", actionType, data, url, session);
      self
        .getActionBodyType(actionType, data)
        .then(async (reqKeys) => {
          self.uSelf.log.debug(
            "getActionBodyType:",
            actionType,
            data,
            url,
            session,
            reqKeys
          );
          switch (actionType) {
            case DeviceActionType.specialAction:
              return resolve(
                `<${reqKeys.request} >${(data as any).xml}</${reqKeys.request}>`
              );
            case DeviceActionType.reboot:
              return resolve(`<${reqKeys.request} />`);
            case DeviceActionType.getRPCMethods:
              return resolve(
                `<${reqKeys.request}><CommandKey></CommandKey></${reqKeys.request}>`
              );
            case DeviceActionType.downloadExternalFile:
              return resolve(
                `<${reqKeys.request}><CommandKey>${
                  (data as any).commandKey || ''
                }</CommandKey><FileType>${
                  (data as any).fileType
                }</FileType><URL>${(data as any).url}</URL><TargetFileName>${
                  (data as any).filename
                }</TargetFileName><DelaySeconds>0</DelaySeconds></${
                  reqKeys.request
                }>`
              );
            case DeviceActionType.downloadFile:
              let dataAs = await self.uSelf.DB.Files.getFileById(
                data as unknown as string,
                docSession
              );
              //self.addFault(null, null, `<${ reqKeys.request }><CommandKey></CommandKey><FileType>${ dataAs.fileType }</FileType><URL>${ url
              //  }/LCF/${ session.id }/${ dataAs.hash }/${ dataAs.filename }</URL><Username>${ dataAs.id }</Username><Password>${ dataAs.id }</Password><FileSize>${ dataAs.fileSize || "0"
              //  }</FileSize><TargetFileName>${ dataAs.filename }</TargetFileName><DelaySeconds>5</DelaySeconds></${ reqKeys.request }>`);
              return resolve(
                `<${reqKeys.request}><CommandKey>${self.uSelf.getMetaField(
                  dataAs,
                  "CommandKey",
                  ""
                )}</CommandKey><FileType>${
                  dataAs.fileType
                }</FileType><URL>${url}/LCF/${session.id}/${dataAs.hash}/${
                  dataAs.filename
                }</URL><Username>${dataAs.id}</Username><Password>${
                  dataAs.id
                }</Password><FileSize>${
                  dataAs.fileSize || "0"
                }</FileSize><TargetFileName>${
                  dataAs.filename
                }</TargetFileName><DelaySeconds>0</DelaySeconds></${
                  reqKeys.request
                }>`
              );
            // <SuccessURL>${encodeEntities(              methodRequest.successUrl || ""              )}</SuccessURL><FailureURL>${encodeEntities(                methodRequest.failureUrl || ""              )}</FailureURL>
            case DeviceActionType.uploadFile:
              let dataAsU = await self.uSelf.DB.UFiles.readyUploadFile(
                session.deviceId!,
                session.id,
                session.groupId!,
                data as unknown as TRFileType,
                docSession
              );
              self.uSelf.log.debug("DeviceActionType.uploadFile", dataAsU);
              let uAp = dataAsU.key.split("+");
              return resolve(
                `<${reqKeys.request}><CommandKey>${self.uSelf.getMetaField(
                  dataAsU,
                  "CommandKey",
                  ""
                )}</CommandKey><FileType>${data}</FileType><URL>${url}/LUF/${
                  session.id
                }/${dataAsU.id}</URL><Username>${uAp[0]}</Username><Password>${
                  uAp[1]
                }</Password><DelaySeconds>5</DelaySeconds></${reqKeys.request}>`
              );

            case DeviceActionType.setParameterValue:
              return resolve(
                `<${reqKeys.request}>${DSC.getSetParamsDeviceConfig(
                  data as any,
                  session.device!
                )}</${reqKeys.request}>`
              );
            case DeviceActionType.getParameterValue:
              return resolve(
                `<${reqKeys.request}>${DSC.getGetParamsDeviceConfig(
                  data as any,
                  session.device!
                )}</${reqKeys.request}>`
              );

            //self.FaultsLib.newFault(null, null, `<${ reqKeys.request }><CommandKey></CommandKey><FileType>${ data }</FileType><URL>${ url
            //  }/LUF/${ session.id }/${ dataAsU.id }</URL><Username>${ uAp[0] }</Username><Password>${ uAp[1] }</Password><DelaySeconds>5</DelaySeconds></${ reqKeys.request }>`);

            // <SuccessURL>${encodeEntities(              methodRequest.successUrl || ""              )}</SuccessURL><FailureURL>${encodeEntities(                methodRequest.failureUrl || ""              )}</FailureURL>
          }
          reject("UNKNOW ACTION");
        })
        .catch(reject);
    });
  }
  public getActionBodyType(
    actionType: DeviceActionType,
    actionData?: any
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      switch (actionType) {
        case DeviceActionType.reboot:
          return resolve({
            request: "cwmp:Reboot",
            response: "cwmp:RebootResponse",
          });
        case DeviceActionType.downloadFile:
        case DeviceActionType.downloadExternalFile:
          return resolve({
            request: "cwmp:Download",
            response: "cwmp:DownloadResponse",
          });
        case DeviceActionType.uploadFile:
          return resolve({
            request: "cwmp:Upload",
            response: "cwmp:UploadResponse",
          });
        case DeviceActionType.getRPCMethods:
          return resolve({
            request: "cwmp:GetRPCMethods",
            response: "cwmp:GetRPCMethodsResponse",
          });
        case DeviceActionType.setParameterValue:
          return resolve({
            request: "cwmp:SetParameterValues",
            response: "cwmp:SetParameterValuesResponse",
          });
        case DeviceActionType.getParameterValue:
          return resolve({
            request: "cwmp:GetParameterValues",
            response: "cwmp:GetParameterValuesResponse",
          });
        case DeviceActionType.specialAction:
          if (!Tools.isObject(actionData)) return reject('SPECIAL ACTION HAS UNDEFINED DATA');
          if (!Tools.isObject(actionData.body)) return reject('SPECIAL ACTION HAS UNDEFINED DATA/body');
          if (!Tools.isString(actionData.body.request)) return reject('SPECIAL ACTION HAS UNDEFINED DATA/body/request');
          if (!Tools.isString(actionData.body.response)) return reject('SPECIAL ACTION HAS UNDEFINED DATA/body/response');
          return resolve({
            request: actionData.body.request,
            response: actionData.body.response,
          });
      }
      reject("UNKNOW ACTION");
    });
  }
}
