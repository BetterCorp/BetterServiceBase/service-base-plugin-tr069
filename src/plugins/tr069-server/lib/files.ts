import { Tables, TRFile, TRFileResp, TRFileType } from '../../../lib';
import { v4 as UUID } from 'uuid';
import { Tools } from '@bettercorp/tools/lib/Tools';
import * as crypto from 'crypto';
import * as FS from 'fs';
import { IPluginConfigStorage } from '../sec.config';
import * as PATH from 'path';
import { XTRA, Plugin } from '../plugin';
import { IDocumentSession } from 'ravendb';
import { Readable } from 'stream';

export class LibFiles {
  private uSelf!: Plugin;
  constructor(uSelf: Plugin, xtra: XTRA) {
    this.uSelf = uSelf;
  }

  private getFileHash(filePath: string): Promise<string> {
    //const self = this;
    return new Promise(async (resolve, reject) => {
      const storMech = (await this.uSelf.getPluginConfig()).storage;
      if (storMech === IPluginConfigStorage.LOCAL) {
        if (!FS.existsSync(filePath)) return reject(`${ filePath } does not exist!`);

        try {
          // https://gist.github.com/F1LT3R/2e4347a6609c3d0105afce68cd101561
          const hash = crypto.createHash('sha1');
          const rs = FS.createReadStream(filePath);
          rs.on('error', reject);
          rs.on('data', (chunk: any) => hash.update(chunk));
          rs.on('end', () => resolve(hash.digest('hex')));
        } catch (exzc) {
          reject(exzc);
        }
        return;
      }
      if (storMech === IPluginConfigStorage.S3) {
        return reject('getFileHash function not supported with this file storage mech.');
      }
      reject('Unknown Storage Mechanism');
    });
  }
  public addFile(filename: string, filetype: TRFileType, session: IDocumentSession, groupId?: string, deviceId?: string, note?: string): Promise<TRFile> {
    //const self = this;
    return new Promise(async (resolve, reject) => {
      try {
        let newFile: any = {
          id: UUID(),
          added: new Date().getTime(),
          fileType: filetype,
          filename: filename,
          groupId: groupId,
          deviceId: deviceId,
          note: note!,
          hash: null!,
          fileSize: null!,
          path: null!,
          "@metadata": {
            "@collection": Tables.files
          }
        };
        await session.store(newFile);
        await session.saveChanges();

        resolve(newFile);
      } catch (exc) {
        reject(exc);
      }
    });
  }
  public addFileStream(fileId: string, readStream: Readable, session: IDocumentSession): Promise<TRFile> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      try {
        let newFile: any = await session.query({ collection: Tables.files }).whereEquals('id', fileId).single();

        const storMech = (await this.uSelf.getPluginConfig()).storage;
        if (storMech === IPluginConfigStorage.LOCAL) {
          let newDir = PATH.join((await this.uSelf.getPluginConfig()).localIsFullPath ? (await this.uSelf.getPluginConfig()).local : PATH.join(this.uSelf.cwd, (await this.uSelf.getPluginConfig()).local), 'files');
          if (!FS.existsSync(newDir)) FS.mkdirSync(newDir);
          newDir = PATH.join(newDir, newFile.groupId || 'any');
          if (!FS.existsSync(newDir)) FS.mkdirSync(newDir);
          newDir = PATH.join(newDir, newFile.deviceId || 'any');
          if (!FS.existsSync(newDir)) FS.mkdirSync(newDir);
          let newFilePath = PATH.join(newDir, `${ newFile.id }.generic`);
          let outputFile = FS.createWriteStream(newFilePath);
          outputFile.on('error', (e) => {
            reject(e);
          });
          readStream.on('end', async () => {
            newFile.hash = await self.getFileHash(newFilePath);
            newFile.fileSize = FS.statSync(newFilePath).size;
            newFile.path = `files/${ newFile.groupId || 'any' }/${ newFile.deviceId || 'any' }/${ newFile.id }.generic`;

            await session.store(newFile);
            await session.saveChanges();

            resolve(newFile);
          });
          readStream.pipe(outputFile);
          return;
        }
        if (storMech === IPluginConfigStorage.S3) {
          const newFilePath = `files/${ newFile.groupId || 'any' }/${ newFile.deviceId || 'any' }/${ newFile.id }.generic`;
          const minioPath = `${ (await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.path }${ newFilePath }`;
          self.uSelf.S3.putObject((await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.bucket,
            minioPath, readStream, async (error: any, etag: string) => {
              if (error) {
                return self.uSelf.log.error(error);
              }
              newFile.path = newFilePath;
              self.uSelf.S3.statObject((await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.bucket, minioPath, async (e: any, stat: any) => {
                if (e) {
                  return reject(e);
                }
                newFile.hash = stat.etag;
                newFile.fileSize = stat.size;

                await session.store(newFile);
                await session.saveChanges();

                resolve(newFile);
              });
            });
          return;
        }
        reject('Unknown Storage Mechanism');
      } catch (exc) {
        reject(exc);
      }
    });
  }
  public getFileById(id: string, session: IDocumentSession): Promise<TRFileResp> {
    //const self = this;
    return new Promise((resolve, reject) => {
      session.query({ collection: Tables.files }).whereEquals('id', id).single().then(x => {
        let fileAsReq: any = {
          ...x,
          fileStream: null
        };
        return resolve(fileAsReq);
      }).catch((ex: any) => {
        reject(ex);
      });
    });
  }
  public getFile(id: string, hash: String, filename: String, session: IDocumentSession): Promise<TRFileResp> {
    const self = this;
    return new Promise((resolve, reject) => {
      session.query<TRFile>({ collection: Tables.files })
        .whereEquals('id', id)
        .whereEquals('hash', hash)
        .whereEquals('filename', filename)
        .single().then(async (file: TRFile | null | any) => {
        if (Tools.isNullOrUndefined(file)) return reject('File not found! ' + id);

        const storMech = (await this.uSelf.getPluginConfig()).storage;
        if (storMech === IPluginConfigStorage.LOCAL) {
          let fileAsReq: TRFileResp = {
            ...file!,
            fileStream: FS.createReadStream(PATH.join((await self.uSelf.getPluginConfig()).localIsFullPath ? (await self.uSelf.getPluginConfig()).local : PATH.join(self.uSelf.cwd, (await this.uSelf.getPluginConfig()).local), file!.path))
          };
          return resolve(fileAsReq);
        }
        if (storMech === IPluginConfigStorage.S3) {
          const minioPath = `${ (await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.path }${ file!.path }`;
          self.uSelf.S3.getObject((await self.uSelf.getPluginConfig(self.uSelf.pluginName)).S3.bucket, minioPath, {}, (err: any, dataStream: any) => {
            if (err) {
              return self.uSelf.log.error(err)
            }
            let fileAsReq: TRFileResp = {
              ...file!,
              fileStream: dataStream
            };
            return resolve(fileAsReq);
          });
          return;
        }
        reject('Unknown Storage Mechanism');
      }).catch((ex: any) => {
        reject(ex);
      });
    });
  }
}
