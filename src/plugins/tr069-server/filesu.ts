//import { Tools } from '@bettercorp/tools/lib/Tools';
import { XTRA, Plugin } from './plugin';

export class UFiles {
  async init(uSelf: Plugin, xtra: XTRA): Promise<void> {
    return new Promise((resolve) => {
      uSelf.fastify.put<any, {
        sessionId: string,
        id: string,
      }>('/LUF/:sessionId/:id', async (req, res): Promise<any | void> => {
        const session = uSelf.RavenDB.openSession();
        uSelf.log.info('PUT REQUEST:' + req.url);
        if (req.params === undefined) {
          res.statusCode = 400;
          return res.send();
        }

        uSelf.DB.UFiles.confirmUploadFile(xtra.cleanString(req.params.sessionId), xtra.cleanString(req.params.id), req.raw, session).then(async (x) => {
          res.statusCode = 201;
          return res.send();
          //res.sendStatus(201);
        }).catch(x => {
          uSelf.log.error(x);
          res.statusCode = 500;
          return res.send();
          //res.sendStatus(500);
        });
      });
      /*uSelf.express.get('/LFS/:groupId/:deviceId/:fileId/:hash', async (req: ERequest, res: EResponse): Promise<any | void> => {
        const session = uSelf.RavenDB.openSession();
        try {
          let myFile = await uSelf.DB.UFiles.getFile(xtra.cleanString(req.params.groupId), xtra.cleanString(req.params.deviceId), xtra.cleanString(req.params.fileId), xtra.cleanString(req.params.hash), session);
          if (Tools.isNullOrUndefined(myFile)) return res.sendStatus(404);
          res.setHeader("content-type", "application/octet-stream");
          myFile.fileStream.pipe(res);
        } catch (exc) {
          uSelf.log.error(exc);
          res.sendStatus(404);
        }
      });*/
      resolve();
    });
  }
};