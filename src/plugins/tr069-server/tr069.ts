import { CPluginClient, IPlugin } from '@bettercorp/service-base/lib/interfaces/plugins';
import { Readable } from 'stream';
import { Device, DeviceAction, Fault, IBCDeviceActionR, ITR069EAREvents, ITR069Events, TR069ConnectionInfo, TR069EARAddFile, TR069EARGetFile, TR069EARGetFileType, TR069EARGetUploadFileUrl, TR069EARGetUploadFileUrlResponse, TRFile, TRFileResp, TRFileType, TRUploadedFile } from '../../lib';
import { IPluginConfig } from './sec.config';

export class tr069 extends CPluginClient<IPluginConfig> {
  public readonly _pluginName: string = "tr069-server";

  public devices: tr069Devices;
  public groups: tr069Groups;
  public actions: tr069Actions;
  public events: tr069Events;
  public faults: tr069Faults;
  public files: tr069Files;
  constructor(uSelf: IPlugin) {
    super(uSelf);
    this.devices = new tr069Devices(uSelf);
    this.groups = new tr069Groups(uSelf);
    this.actions = new tr069Actions(uSelf);
    this.events = new tr069Events(uSelf);
    this.faults = new tr069Faults(uSelf);
    this.files = new tr069Files(uSelf);
  }
}

export interface tr069UploadedFile extends TRUploadedFile {
  downUrl: string;
}
export interface tr069FileDownloaded extends TRFileResp {
  time: number;
  ip: string;
}
export interface tr069DeviceInformed {
  deviceId: string;
  groupId: string;
  time: number;
  ip: string;
}
export class tr069Events extends CPluginClient<IPluginConfig> {
  public readonly _pluginName: string = "tr069-server";

  public async deviceFileDownloaded(listener: { (data: tr069FileDownloaded): Promise<void>; }) {
    await this.onEvent(ITR069Events.DeviceFileDownloaded, listener);
  }
  public async deviceInformed(listener: { (data: tr069DeviceInformed): Promise<void>; }) {
    await this.onEvent(ITR069Events.DeviceInformed, listener);
  }
  public async deviceActionAdded(listener: { (data: DeviceAction<any>): Promise<void>; }) {
    await this.onEvent(ITR069Events.DeviceActionAdded, listener);
  }
  public async deviceActionCompleted(listener: { (data: DeviceAction<any>): Promise<void>; }) {
    await this.onEvent(ITR069Events.DeviceActionCompleted, listener);
  }
  public async deviceActionTriggered(listener: { (data: DeviceAction<any>): Promise<void>; }) {
    await this.onEvent(ITR069Events.DeviceActionTriggered, listener);
  }
  public async deviceAdded(listener: { (data: Device): Promise<void>; }) {
    await this.onEvent(ITR069Events.DeviceAdded, listener);
  }
  public async deviceAdopted(listener: { (data: Device): Promise<void>; }) {
    await this.onEvent(ITR069Events.DeviceAdopted, listener);
  }
  public async faultReceived(listener: { (data: Fault): Promise<void>; }) {
    await this.onEvent(ITR069Events.FaultReceived, listener);
  }
  public async deviceFileUploaded(listener: { (data: tr069UploadedFile): Promise<void>; }) {
    await this.onEvent(ITR069Events.DeviceFileUploaded, listener);
  }
}

export class tr069Devices extends CPluginClient<IPluginConfig> {
  public readonly _pluginName: string = "tr069-server";

  public get(id: string): Promise<Device> {
    return this.emitEventAndReturn<any, Device>(ITR069EAREvents.GetDevice, {
      id
    });
  }
  public adopt(id: string): Promise<void> {
    return this.emitEventAndReturn<any, void>(ITR069EAREvents.AdoptDevice, {
      id
    });
  }
}

export class tr069Groups extends CPluginClient<IPluginConfig> {
  public readonly _pluginName: string = "tr069-server";

  public update(sourceGroupId: string, sourcePlugin: string, autoAdopt: boolean): Promise<string> {
    return this.emitEventAndReturn<any, string>(ITR069EAREvents.AddUpdateGroup, {
      id: sourceGroupId,
      sourcePlugin,
      autoAdopt
    });
  }
  public getConnectionInformation(sourceGroupId: string, sourcePlugin: string): Promise<TR069ConnectionInfo> {
    return this.emitEventAndReturn<any, TR069ConnectionInfo>(ITR069EAREvents.GetGroupConnection, {
      id: sourceGroupId,
      sourcePlugin
    });
  }
}

export class tr069Actions extends CPluginClient<IPluginConfig> {
  public readonly _pluginName: string = "tr069-server";

  public add<T = any>(deviceId: string, groupId: string, action: IBCDeviceActionR<T>): Promise<DeviceAction<T>> {
    return this.emitEventAndReturn<{
      deviceId: string, groupId: string, action: IBCDeviceActionR<T>;
    }, DeviceAction<T>>(ITR069EAREvents.AddAction, {
      deviceId,
      groupId,
      action
    });
  }
}

export class tr069Faults extends CPluginClient<IPluginConfig> {
  public readonly _pluginName: string = "tr069-server";

  /*public add<T = any>(deviceId: string, groupId: string, action: DeviceAction<T>): Promise<DeviceAction<T>> {
    return this.emitEventAndReturn<any, DeviceAction<T>>(ITR069EAREvents.AddAction, {
      deviceId,
      groupId,
      action
    });
  }*/
}

export class tr069Files extends CPluginClient<IPluginConfig> {
  public readonly _pluginName: string = "tr069-server";

  /*public add<T = any>(deviceId: string, groupId: string, action: DeviceAction<T>): Promise<DeviceAction<T>> {
    return this.emitEventAndReturn<any, DeviceAction<T>>(ITR069EAREvents.AddAction, {
      deviceId,
      groupId,
      action
    });
  }*/
  public readyForDeviceUpload(type: TRFileType,
    groupId: string,
    deviceId: string): Promise<TR069EARGetUploadFileUrlResponse> {
    return this.emitEventAndReturn<TR069EARGetUploadFileUrl, TR069EARGetUploadFileUrlResponse>(ITR069EAREvents.GetUploadFileUrl, {
      groupId,
      type,
      deviceId,
    });
  }

  public add(filename: string,
    type: TRFileType,
    stream: Readable,
    groupId?: string,
    deviceId?: string,
    note?: string): Promise<TRFile> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      try {
        const createdFile = await this.emitEventAndReturn<TR069EARAddFile, TRFile>(ITR069EAREvents.AddFile, {
          filename,
          groupId,
          type,
          deviceId,
          note
        });
        const streamId = await this.emitEventAndReturn<string, string>(ITR069EAREvents.AddFileStream, createdFile.id, 60);
        await self.sendStream(streamId, stream);
        resolve(createdFile);
      } catch (exc) {
        reject(exc);
      }
    });
  }

  public get(type: TR069EARGetFileType, filename: String,
    key: String,
    deviceId: string,
    id: string,
    groupId: string,
    tenantId: string,): Promise<Readable> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      const streamId = await self.receiveStream(async (error, stream) => {
        if (error) return reject(error);
        resolve(stream);
      }, 60);
      self.emitEventAndReturn<TR069EARGetFile, string>(ITR069EAREvents.GetFile, {
        fileStreamId: streamId,
        filename,
        type,
        key,
        deviceId,
        id,
        groupId,
        tenantId
      }, 65).then(() => { }).catch(reject);
    });
  }
}
