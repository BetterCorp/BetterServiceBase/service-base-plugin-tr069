import { IDictionary } from '@bettercorp/tools/lib/Interfaces';
import { DeviceInfo, Session, XMLItem } from '../../lib';

export class DSC {
  /*public static getDeviceInformInformation(informData: any, device: DeviceInfo): any {
    let returnData: any = {};
    switch (device.manufacturer.toLowerCase()) {
      case "yealink": 
      case "mikrotik": {
        keysToSet.push({
          name: 'Device.ManagementServer.PeriodicInformInterval',
          value: informTime
        });
        keysToSet.push({
          name: 'Device.ManagementServer.ConnectionRequestUsername',
          value: username
        });
        keysToSet.push({
          name: 'Device.ManagementServer.ConnectionRequestPassword',
          value: password
        });
      } break;
      case "fanvil": {
        keysToSet.push({
          name: 'InternetGatewayDevice.ManagementServer.PeriodicInformInterval',
          value: informTime
        });
        keysToSet.push({
          name: 'InternetGatewayDevice.ManagementServer.ConnectionRequestUsername',
          value: username
        });
        keysToSet.push({
          name: 'InternetGatewayDevice.ManagementServer.ConnectionRequestPassword',
          value: password
        });
      } break;
    }
    return returnData;
  }*/
  public static GetDeviceParamKey(key: string, device: DeviceInfo): string {
    let knownParams: IDictionary<string> = {
      'Device.ManagementServer.PeriodicInformInterval': 'Device.ManagementServer.PeriodicInformInterval',
      'Device.ManagementServer.ConnectionRequestUsername': 'Device.ManagementServer.ConnectionRequestUsername',
      'Device.ManagementServer.ConnectionRequestPassword': 'Device.ManagementServer.ConnectionRequestPassword',
    };
    switch (device.manufacturer.toLowerCase()) {
      case "fanvil": {
        knownParams = {
          'Device.ManagementServer.PeriodicInformInterval': 'InternetGatewayDevice.ManagementServer.PeriodicInformInterval',
          'Device.ManagementServer.ConnectionRequestUsername': 'InternetGatewayDevice.ManagementServer.ConnectionRequestUsername',
          'Device.ManagementServer.ConnectionRequestPassword': 'InternetGatewayDevice.ManagementServer.ConnectionRequestPassword',
        };
      } break;
    }
    return knownParams[key] || key;
  }
  public static SetDeviceConnectionDetails(username: string, password: string, informTime: number, device: DeviceInfo, session: Session): string {
    let keysToSet: Array<XMLItem> = [];

    keysToSet.push({
      name: 'Device.ManagementServer.PeriodicInformInterval',
      value: informTime
    });
    keysToSet.push({
      name: 'Device.ManagementServer.ConnectionRequestUsername',
      value: username
    });
    keysToSet.push({
      name: 'Device.ManagementServer.ConnectionRequestPassword',
      value: password
    });

    return this.getXMLConfig(keysToSet, session);
  }

  public static getXMLConfig(arrayOfItems: Array<XMLItem>, session: Session) {
    let output = `<?xml version="1.0" encoding="UTF-8"?>
              <${ session.envKeyType }:Envelope xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:${ session.envKeyType }="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cwmp="urn:dslforum-org:cwmp-1-0">
                  <${ session.envKeyType }:Header>
                      <cwmp:ID ${ session.envKeyType }:mustUnderstand="1">${ session.trKey }</cwmp:ID>
                  </${ session.envKeyType }:Header>
                  <${ session.envKeyType }:Body>
                    ${ this.getSetParamsDeviceConfig(arrayOfItems, session.device!) }
                  </${ session.envKeyType }:Body>
              </${ session.envKeyType }:Envelope>`;
    return output;
  }
  public static getSetParamsDeviceConfig(arrayOfItems: Array<XMLItem>, device: DeviceInfo) {
    let output = `<cwmp:SetParameterValues>
                      <ParameterList soap-enc:arrayType="cwmp:ParameterValueStruct[${ arrayOfItems.length }]">`;
    for (let item of arrayOfItems)
      output += `
                        <ParameterValueStruct>
                            <Name>${ this.GetDeviceParamKey(item.name, device) }</Name>
                            <Value xsi:type="xsd:${ typeof item.value || 'string' }">${ item.value }</Value>
                        </ParameterValueStruct>`;

    output += `</ParameterList>    
                    </cwmp:SetParameterValues>`;
    return output;
  }
  public static getGetParamsDeviceConfig(arrayOfItems: Array<XMLItem>, device: DeviceInfo) {
    let output = `<cwmp:GetParameterValues>
                      <ParameterNames soap-enc:arrayType="xsd:string[${ arrayOfItems.length }]">`;
    for (let item of arrayOfItems)
      output += `<string>${ this.GetDeviceParamKey(item.name, device) }</string>`;

    output += `</ParameterNames>    
                    </cwmp:GetParameterValues>`;
    return output;
  }
}